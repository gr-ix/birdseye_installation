"""
Fabric script to download and install BirdsEye

Examples:
fab -f birdseye.py -H rsx.gr-ix.gr birdseye
fab -f birdseye.py -H rsx.gr-ix.gr --set vlan=vlan400 birdseye
fab --set version=1.1.0 -f birdseye.py -u aduitsis --hosts rsx.gr-ix.gr birdseye birdseye_conf patch
fab --set version=1.1.0,vlan=vlan400 -f birdseye.py -u aduitsis --hosts rsx.gr-ix.gr birdseye birdseye_conf patch
"""

from fabric.api import *
from fabric.contrib.files import upload_template
from fabric.colors import *

if 'version' not in env:
    env.version = '1.1.4'
env.extension = 'zip'
env.where = '/srv'
env.www_user = 'www-data'
env.temp = '/var/tmp'
env.apachedir = '/etc/apache2/sites-enabled'
env.defaultfile = 'birdseye.conf'
env.birdseye = 'birdseye-'
if 'vlan' not in env:
    env.vlan = 'vlan505'
env.timezone = 'Europe/Athens'
env.cleanup_script='cleanup_cache.sh'
if 'email' not in env:
    env.email = 'aduitsis@noc.ntua.gr'


def where():
    return env.where


def extension():
    return env.extension


def version():
    return env.version


def dirname():
    return env.birdseye + version()


def tarball():
    return dirname() + '.' + extension()


def temp():
    return env.temp


def apacheconf():
    return env.apachedir + '/' + env.defaultfile


def url():
    return 'https://github.com/inex/birdseye/archive/refs/tags/v' + version(
    ) + '.' + extension()


def destination():
    return where() + '/' + dirname()


def www_user():
    return env.www_user


def vlan():
    return env.vlan

def timezone():
    return env.timezone

def get_host_part():
    return run('hostname').split('-')[0]

@task
def birdseye():
    """
    Install BirdsEye

    Downloads BirdsEye tarball, upacks it and sets it up in an appropriate location
    along with its accompanying apache configuration
    """
    with cd(temp()):
        run('wget -c ' + url())
    sudo('test -d ' + destination() + ' || unzip ' + temp() + '/v'+version()+'.' +
         extension() + ' -d ' + where())
    sudo('chown -R ' + www_user() + ' ' + destination() + '/storage')
    sudo("cd "+destination()+" && composer install")
    birdseye_conf()
    setup_web_server()

@task
def birdseye_conf():
    host_part = get_host_part()
    upload_template(
        'birdseye.env.j2',
        destination() + '/' + 'birdseye-' + host_part + '-' + vlan() +
        '-ipv4.env',
        use_sudo=True,
        use_jinja=True,
        context={
            'hostname': host_part + '.gr-ix.gr',
            'birdc': '/usr/sbin/birdc -r -s /run/bird/bird.ctl'
        })
    upload_template(
        'birdseye.env.j2',
        destination() + '/' + 'birdseye-' + host_part + '-' + vlan() +
        '-ipv6.env',
        use_sudo=True,
        use_jinja=True,
        context={
            'hostname': host_part + '.gr-ix.gr',
            'birdc': '/usr/sbin/birdc -r -s /run/bird/bird6.ctl',
            'timezone': timezone()
        })

@task
def setup_web_server():
    upload_template(
        'birdseye.conf.j2',
        apacheconf(),
        use_sudo=True,
        use_jinja=True,
        context={'destdir': destination()})
    sudo('a2enmod rewrite')
    # finally, allow www-data to execute birdc and open its ctrl socket
    sudo('usermod -a -G bird ' + www_user())
    # apache restart must happen *AFTER* the usermod command
    # otherwise, it doesn't work. Obviously, the changes affect only
    # processes spawned after the usermod has taken place
    sudo('service apache2 restart')

@task
def setup_rotation():
    """
    Install a script that rotates the laravel cache in Birdseye
    """
    full_path=destination() + '/' + env.cleanup_script
    upload_template(
        env.cleanup_script+'.j2',
        full_path,
        use_sudo=True,
        mode='0744',
        use_jinja=True,
        context={'email':env.email}
        )
    sudo('chown '+www_user()+' '+full_path)
    upload_template(
        'birdseye_cleanup_cache.j2',
        '/etc/cron.d/birdseye_cleanup_cache',
        use_sudo=True,
        mode='0644',
        use_jinja=True,
        context={
            'email':env.email,
            'full_path': full_path,
            'www_user': www_user()
        })
    sudo('chown '+'root'+': '+'/etc/cron.d/birdseye_cleanup_cache')

@task
def patch():
    patch_Routes()
    patch_routes()
    patch_bird()

@task
def patch_Routes():
    """
    Patch the Routes.php file for Birdseye 1.2
    """
    upload_template(
            'Routes.php',
            destination()+'/app/Http/Controllers/Routes.php',
            use_sudo=True,
            mode='0744',
            use_jinja=True,
            context={}
            )

@task
def patch_bird():
    """
    Patch the Bird.php file for Birdseye 1.2
    """
    upload_template(
            'Bird.php',
            destination()+'/app/Bird.php',
            use_sudo=True,
            mode='0744',
            use_jinja=True,
            context={}
            )
@task
def patch_routes():
    """
    Patch the routes.php file for Birdseye 1.2
    """
    upload_template(
            'routes.php',
            destination()+'/app/Http/routes.php',
            use_sudo=True,
            mode='0744',
            use_jinja=True,
            context={}
            )

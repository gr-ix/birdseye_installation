This repository is used to install birdseye to a Bird-based Route Server via fabric.
The process of installation remains manual to avoid puppet intervention with regards to the already opeartional route servers.

Versions (fab --version)
Fabric 1.15.0
Paramiko 2.6.0

Installation (e.g. for athens POP) follows the process below and MUST RUN FROM A REMOTE MACHINE AND NOT THE ROUTE SERVER:

This installs birdseye

- /home/marinos/.local/bin/fab --set version=1.2.1,vlan=vlan505 -f birdseye.py -u mdimolianis --hosts rs2.gr-ix.gr birdseye

This patches specific files for GR-IX tooling

- /home/marinos/.local/bin/fab --set version=1.2.1,vlan=vlan505 -f birdseye.py -u mdimolianis --hosts rs2.gr-ix.gr patch
